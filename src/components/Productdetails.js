import React, { Component } from 'react';
import Category from './Category';
import Productlist from './Productlist';

export default class Productdetails extends Component {
    constructor(props){
        super(props);
    }

   
    render() {
        //  this.category();
        return (
            <div>
            {
                (this.props.product)?<Productlist product={this.props.product}/>:null
            }
                
            </div>
        )
    }
}
// ?