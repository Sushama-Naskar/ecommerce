import React, { Component } from 'react';
import './productlist.css';
import Man from '../images/men.jpg';
import Productheader from './Productheader';
import Star from '../images/star.png';

export default class Productlist extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        console.log(this.props.product);
        return (
            <div className="productlist">
                <Productheader />
                {
                    (this.props.product) ? <div>

                        <div className='margin-top'>
                            {
                                this.props.product.map((element) => {
                                    return (
                                        <div className="card" key={element.id}> 
                                            <div className="product-div ">
                                               <div className='image-align'> <img src={element.image} className='product-image' /></div>

                                                <div class="product-name-price">
                                                    <div className='name'> {element.title}</div>
                                                    <div className='product-description'>{element.description}</div>

                                                    <div className='price'>${element.price}</div>
                                                    <div className='rating'><div>{element.rating.rate}</div><div><img src={Star} className='star' /></div><div>({element.rating.count})</div></div>

                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }










                        </div>
                    </div> : null

                }


            </div>
        )
    }
}
// {
//     (this.props.product)?
//     this.props.product.map((element)=>{
//         return(
//             <div key={element.id}>
//             <div>{element.category}</div>
//             </div>

//         )

//     }):null
// }

// <div key={element.id} className="card-2">
// <div className="image">
//   <img src={element.image} className="img" />
// </div>
// <div className="details">
//   <h1>{element.title}</h1>
//   <p>{element.description}</p>
//   <p>{element.price}</p>
//   <p>Rate:{element.rating.rate}</p>
//   <p>Count:{element.rating.count}</p>
// </div>
// </div>

// <img src={Man} className='product-heading-image'/>


<div className='product-heading-image-div'>
    <img src={Man} className='product-heading-image' />
</div>