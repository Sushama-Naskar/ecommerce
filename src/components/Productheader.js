import React, { Component } from 'react';
import './header.css';
import Headerimage from '../images/header1.jpg'
import Founderimage from '../images/founder.jpg';
import Quote from '../images/chat.png';
import {Link} from 'react-router-dom';
import Man from '../images/men.jpg';

export default class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="left-div">
                    <div>
                        <div className='red-color'>Hey ,Welcome to Muse+Meta !</div>
                        <div className='font-size-40'>Discover Trends.</div>
                        <div>
                            <div className='left-image'>
                                <div className='image-name'>
                                    <div><img src={Founderimage} className='Founder-image' /></div>
                                    <div className="founder-name"><div className='name'>Jennier Walter</div><div className='font-color-grey' >Customer</div></div>
                                </div>
                                <div className="width-70"><div><img src={Quote} className='quote'/></div><div className='text-align-left'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div></div>
                            </div>

                        </div>
                        <div><Link to="/Home" className='home-text-decoration'><button>Home</button></Link></div>
                    </div>
                </div>
                <div className='image-section'><img src={Man} className='header-image' /></div>

            </div>
        )
    }
}
