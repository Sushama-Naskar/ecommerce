import React, { Component } from 'react'
import Nav from './Nav';
import Header from './Header';
import Category from './Category';
import Productdetails from './Productdetails';
import Cssloader from './Cssloader';

export default class Product extends Component {
    constructor(props){
        super(props);
        this.state={
          product:null,
          productReceived:false,
          apierror:false,
          dataloading:true
    
        }
      }
    
      componentDidMount=()=>{
        fetch('https://fakestoreapi.com/products')
                .then(res=>{
                  return res.json();
                })
                .then(data=>{
                  // console.log(data);
                 this.setState({
                  product:data,
                  productReceived:true,
                  
                  dataloading:false
                 })
    
                }).catch((err)=>{
                  console.log(err);
                 
                  this.setState({
                    apierror:true,
                    dataloading:false
                   })
                })
      }
//    category=()=>{
//        let category=this.state.product.map((Element)=>{
//            return Element.category;
//        }).sort();
//        let uniqueCategory=category.filter((Element,index)=>{ return category.indexOf(Element)===index});
//        console.log(uniqueCategory);
//    }
   
    render() {
        console.log(this.state.product);
        // this.category();
        return (
            <div>
             { (this.state.dataloading===true)?
              <Cssloader/>
              : (this.state.apierror==true)?<h1>Api Error</h1>:(this.state.product===null)?<h1>No Product</h1>:<Productdetails product={this.state.product}/>
              }
                
            </div>
        )
    }
}

