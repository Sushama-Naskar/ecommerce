import React, { Component } from 'react'
import Nav from './Nav'
import Header from './Header'
import Category from './Category'
import Footer from './Footer'
import About from './About'
import Aboutproduct from './Aboutproduct'

export default class Home extends Component {
    render() {
        return (
            <div>
            
            <Header/>
            <Aboutproduct/>
            <Category/>
            <About/>
            <Footer/>
            </div>
        )
    }
}
